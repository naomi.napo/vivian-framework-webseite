# VivianFrameworkWebseite

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.3.

# Start with the Project

Fork / Clone the project and open your code editor. You need to have the package manager npm. (https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

After downloading npm, run `npm install` to get all the dependencies and packages needed for the project.

## Development server

Run `ng serve` for a dev server of the application. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding / Writting 

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

Angular have a full documentation on how it works. Don't hesitate to check it (https://angular.io/docs)

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
