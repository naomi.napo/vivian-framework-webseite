import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { faLaugh, faTools, faClock } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  faClock = faClock;
  faTools = faTools;
  faLaugh = faLaugh;

  @Output() setMenuItem = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    }
}
