import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public menuItems: string[] = ["Vivian Framework", "Tutorials", "Projects","Download", "Contact"];
  public activeMenuItem: string = "Vivian Framework";

  constructor() { }
  
  ngOnInit(): void {
  }

  public setActiveMenuItem(clickedMenuItem: string) {
    this.activeMenuItem = clickedMenuItem;
  }

}
