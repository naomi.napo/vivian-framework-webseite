import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { faArrowUp } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-tutorials',
  templateUrl: './tutorials.component.html',
  styleUrls: ['./tutorials.component.scss']
})
export class TutorialsComponent implements OnInit {

  arrowUpIcon = faArrowUp;

  @ViewChild('folderStructure') folderStructure: ElementRef;
  @ViewChild('importPrototype') importPrototype: ElementRef;
  @ViewChild('button') button: ElementRef;
  @ViewChild('slider') slider: ElementRef;
  @ViewChild('touchscreen') touchscreen: ElementRef;
  @ViewChild('states') states: ElementRef;
  @ViewChild('transitions') transitions: ElementRef;


  public listItems: string[] = ["Folder structure", "Import prototype", "Button", "Slider", "Touchscreen", "States", "Transitions"];
  public activeItem: string = "Folder structure";
  private folderStructurePos: number;
  private importPrototypePos: number;
  private buttonPos: number;
  private sliderPos: number;
  private touchscreenPos: number;
  private statesPos: number;
  private transitionsPos: number;
  private itemClicked: boolean = false;


  constructor() { }

  ngOnInit(): void {
  }

  public setActiveItem(clickedItem: string) {
    this.itemClicked = true;
    this.activeItem = clickedItem;
  }

  public scrollToElement(clickedItem: string) {
    const listElement = document.getElementById(clickedItem);
    listElement.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    // Timeout weil sonst onWindowScroll trotz if-Bedingung einsetzt und dann im Menü der Balken hin- und herspringt
    setTimeout( () => { this.itemClicked = false }, 1000 );
  }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    if(!this.itemClicked) {
      const currentTopPos = window.scrollY;
      this.getPositions();
      if (currentTopPos > this.folderStructurePos) {
        this.activeItem = this.listItems[0];
      } 
      if (currentTopPos > this.importPrototypePos) {
        this.activeItem = this.listItems[1];
      }
      if (currentTopPos > this.buttonPos) {
        this.activeItem = this.listItems[2];
      }
      if (currentTopPos > this.sliderPos) {
        this.activeItem = this.listItems[3];
      }
      if (currentTopPos > this.touchscreenPos) {
        this.activeItem = this.listItems[4];
      }
      if (currentTopPos > this.statesPos) {
        this.activeItem = this.listItems[5];
      }
      if (currentTopPos > this.transitionsPos) {
        this.activeItem = this.listItems[6];
      }
    }
  }

  private getPositions() {
    this.folderStructurePos = this.folderStructure.nativeElement.offsetTop;
    this.importPrototypePos = this.importPrototype.nativeElement.offsetTop;
    this.buttonPos = this.button.nativeElement.offsetTop;
    this.sliderPos = this.slider.nativeElement.offsetTop;
    this.touchscreenPos = this.touchscreen.nativeElement.offsetTop;
    this.statesPos = this.states.nativeElement.offsetTop;
    this.transitionsPos = this.transitions.nativeElement.offsetTop;
  }

  public scrollUp() {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }
}
