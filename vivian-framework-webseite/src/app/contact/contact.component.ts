import { Component, OnInit, ViewChild } from '@angular/core';
import { NgModel } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  @ViewChild('emailInput') emailInput: NgModel;

  public firstName: string;
  public surname: string;
  public email: string;
  public validEmail: boolean = true;
  public message: string;
  public phone: number;

  constructor() { }

  ngOnInit(): void {
  }

  public async checkForValidEmail() {
    if(this.emailInput.errors?.email) {
      this.validEmail = false;
    } else {
      this.validEmail = true;
    }
  }

}
