import { Component, OnInit } from '@angular/core';
import { faDownload } from '@fortawesome/free-solid-svg-icons';
import { faGitlab } from '@fortawesome/free-brands-svg-icons';


@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss']
})
export class DownloadComponent implements OnInit {

  downloadIcon = faDownload;
  gitlabIcon = faGitlab;
  
  constructor() { }

  ngOnInit(): void {
  }

}
