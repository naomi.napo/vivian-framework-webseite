import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VivianFrameworkComponent } from './vivian-framework.component';

describe('VivianFrameworkComponent', () => {
  let component: VivianFrameworkComponent;
  let fixture: ComponentFixture<VivianFrameworkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VivianFrameworkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VivianFrameworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
