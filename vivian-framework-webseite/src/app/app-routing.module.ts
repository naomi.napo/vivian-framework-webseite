import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { DownloadComponent } from './download/download.component';
import { HomePageComponent } from './home-page/home-page.component';
import { ProjectsComponent } from './projects/projects.component';
import { TutorialsComponent } from './tutorials/tutorials.component';

const routes: Routes = [
  {path: 'Tutorials', component: TutorialsComponent},
  {path: 'home', component: HomePageComponent},
  {path: '', component: HomePageComponent},
  {path: 'Contact', component: ContactComponent},
  {path: 'Download', component: DownloadComponent},
  {path: 'Projects', component: ProjectsComponent},
  {path: 'VivianFramework', component: HomePageComponent},
  {path: 'Download', component: DownloadComponent}
  // Vvian Framework component,
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
